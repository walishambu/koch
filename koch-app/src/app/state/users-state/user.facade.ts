import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { userQuery } from './user.selector';
import { UserState } from './user.reducer';
import * as actions from './user.action';
import { UserModel } from 'src/app/shared';

@Injectable()
export class UserFacade {
  userList$: Observable<any>;

  constructor(private store: Store<UserState>) {
    this.userList$ = this.store.pipe(select(userQuery.getUsers));
  }

  getUsers(): void {
    this.store.dispatch(new actions.GetUsers());
  }

  saveUser(data: UserModel): void {
    this.store.dispatch(new actions.SaveUser(data));
  }

  updateUser(data: UserModel): void {
    this.store.dispatch(new actions.UpdateUser(data));
  }

  deleteUser(userId: number): void {
    this.store.dispatch(new actions.DeleteUser(userId));
  }

  saveHieararchy(data: object): void {
    this.store.dispatch(new actions.SaveHierarchy(data));
  }
}
