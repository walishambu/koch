import * as actions from './user.action';
import { UserModel } from 'src/app/shared';

export interface UserState {
  users: UserModel[];
}

export const initialState: UserState = {
  users: [],
};

export function reducer(
  state: UserState = initialState,
  action: actions.UsersAction
): UserState {
  switch (action.type) {
    case actions.UserActionTypes.GET_USERS: {
      // initialState;
      state = {
        ...state,
        users: state.users,
      };
      break;
    }

    case actions.UserActionTypes.GET_USER_SUCCESS: {
      initialState;
      state = {
        ...state,
        users: action.payload,
      };
      break;
    }

    case actions.UserActionTypes.SAVE_USER_SUCCESS: {
      initialState;
      state = {
        ...state,
        users: [],
      };
      break;
    }

    case actions.UserActionTypes.UPDATE_USER_SUCCESS: {
      initialState;
      state = {
        ...state,
        users: [],
      };
      break;
    }

    case actions.UserActionTypes.DELETE_USER_SUCCESS: {
      initialState;
      state = {
        ...state,
        users: [],
      };
      break;
    }
  }
  return state;
}
