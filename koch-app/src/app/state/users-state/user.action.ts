import { Action } from '@ngrx/store';
import { UserModel } from './../../shared';

export enum UserActionTypes {
  GET_USERS = '[User] GET_USERS',
  GET_USER_FAIL = '[User] GET_USER_FAIL',
  GET_USER_SUCCESS = '[User] GET_USER_SUCCESS',

  SAVE_USER = '[User] SAVE_USER',
  SAVE_USER_FAIL = '[User] SAVE_USER_FAIL',
  SAVE_USER_SUCCESS = '[User] SAVE_USER_SUCCESS',

  UPDATE_USER = '[User] UPDATE_USER',
  UPDATE_USER_FAIL = '[User]  UPDATE_USER_FAIL',
  UPDATE_USER_SUCCESS = '[User] UPDATE_USER_SUCCESS',

  DELETE_USER = '[User] DELETE_USER',
  DELETE_USER_FAIL = '[User] DELETE_USER_FAIL',
  DELETE_USER_SUCCESS = '[User] DELETE_USER_SUCCESS',

  SAVE_HIRARCHY = '[User] SAVE_HIRARCHY',
  SAVE_HIRARCHY_FAIL = '[User] SAVE_HIRARCHY_FAIL',
  SAVE_HIRARCHY_SUCCESS = '[User] SAVE_HIRARCHY_SUCCESS',
}

export class GetUsers implements Action {
  readonly type: UserActionTypes.GET_USERS = UserActionTypes.GET_USERS;
  constructor() {}
}

export class GetUsersSuccess implements Action {
  readonly type: UserActionTypes.GET_USER_SUCCESS =
    UserActionTypes.GET_USER_SUCCESS;
  constructor(public payload: UserModel[]) {}
}

//Save

export class SaveUser implements Action {
  readonly type: UserActionTypes.SAVE_USER = UserActionTypes.SAVE_USER;
  constructor(public payload: UserModel) {}
}

export class SaveUserSuccess implements Action {
  readonly type: UserActionTypes.SAVE_USER_SUCCESS =
    UserActionTypes.SAVE_USER_SUCCESS;
  constructor(public payload: UserModel) {}
}

//Update
export class UpdateUser implements Action {
  readonly type: UserActionTypes.UPDATE_USER = UserActionTypes.UPDATE_USER;
  constructor(public payload: UserModel) {}
}

export class UpdateUserSuccess implements Action {
  readonly type: UserActionTypes.UPDATE_USER_SUCCESS =
    UserActionTypes.UPDATE_USER_SUCCESS;
  constructor(public payload: UserModel) {}
}

//Delete

export class DeleteUser implements Action {
  readonly type: UserActionTypes.DELETE_USER = UserActionTypes.DELETE_USER;
  constructor(public payload: number) {}
}

export class DeleteUserSuccess implements Action {
  readonly type: UserActionTypes.DELETE_USER_SUCCESS =
    UserActionTypes.DELETE_USER_SUCCESS;
  constructor() {}
}

export class SaveHierarchy implements Action {
  readonly type: UserActionTypes.SAVE_HIRARCHY = UserActionTypes.SAVE_HIRARCHY;
  constructor(public payload: object) {}
}

export class SaveHierarchySuccess implements Action {
  readonly type: UserActionTypes.SAVE_HIRARCHY_SUCCESS =
    UserActionTypes.SAVE_HIRARCHY_SUCCESS;
  constructor() {}
}

export type UsersAction =
  | GetUsers
  | GetUsersSuccess
  | SaveUser
  | SaveUserSuccess
  | UpdateUser
  | UpdateUserSuccess
  | DeleteUser
  | DeleteUserSuccess
  | SaveHierarchy
  | SaveHierarchySuccess;
