import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

import { UserModel } from 'src/app/shared';

export interface UserState {
  selectedUser: UserModel | null;
  users: UserModel[];
  isLoading?: boolean;
  error?: any;
}

export const initialState: UserState = {
  selectedUser: null,
  users: [],
  isLoading: false,
  error: null,
};
