import { state } from '@angular/animations';
import { createSelector, createFeatureSelector } from '@ngrx/store';
import { UserState } from './user.reducer';

const getUserState = createFeatureSelector<UserState>('users');

const getUsers = createSelector(
  getUserState,
  (state: UserState) => state.users
);

export const userQuery = {
  getUsers,
};
