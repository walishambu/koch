import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect, Effect } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { EMPTY, Observable, of as observableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { UserApiService } from './../../shared';
import * as UserActions from './user.action';
import { UserState } from './user.reducer';

@Injectable()
export class UsersEffects {
  constructor(
    private userService: UserApiService,
    private actions$: Actions,
    private store$: Store<UserState>
  ) {}

  @Effect()
  getUsers$: Observable<Action> = this.actions$.pipe(
    ofType<UserActions.GetUsers>(UserActions.UserActionTypes.GET_USERS),
    switchMap(() => {
      return this.userService
        .getUsers()
        .pipe(map((res) => new UserActions.GetUsersSuccess(res)));
    }),
    catchError((err) => this.showError(err))
  );

  @Effect()
  saveUser$: Observable<Action> = this.actions$.pipe(
    ofType<UserActions.SaveUser>(UserActions.UserActionTypes.SAVE_USER),
    map((action: UserActions.SaveUser) => action.payload),
    switchMap((data) => {
      return this.userService
        .saveUser(data)
        .pipe(map((res) => new UserActions.SaveUserSuccess(res)));
    }),
    catchError((err) => this.showError(err))
  );

  @Effect()
  updateUser$: Observable<Action> = this.actions$.pipe(
    ofType<UserActions.UpdateUser>(UserActions.UserActionTypes.UPDATE_USER),
    map((action: UserActions.UpdateUser) => action.payload),
    switchMap((data) => {
      return this.userService
        .updateUser(data)
        .pipe(map((res) => new UserActions.UpdateUserSuccess(res)));
    }),
    catchError((err) => this.showError(err))
  );

  @Effect()
  deleteUser$: Observable<Action> = this.actions$.pipe(
    ofType<UserActions.DeleteUser>(UserActions.UserActionTypes.DELETE_USER),
    map((action: UserActions.DeleteUser) => action.payload),
    switchMap((userId) => {
      return this.userService
        .deleteUser(userId)
        .pipe(map((res) => new UserActions.DeleteUserSuccess()));
    }),
    catchError((err) => this.showError(err))
  );

  @Effect()
  saveHierarchy$: Observable<Action> = this.actions$.pipe(
    ofType<UserActions.SaveHierarchy>(
      UserActions.UserActionTypes.SAVE_HIRARCHY
    ),
    map((action: UserActions.SaveHierarchy) => action.payload),
    switchMap((hierachyData) => {
      return this.userService
        .saveHierachy(hierachyData)
        .pipe(map((res) => new UserActions.SaveHierarchySuccess()));
    }),
    catchError((err) => this.showError(err))
  );

  showError(err: HttpErrorResponse): Observable<never> {
    return EMPTY;
  }
}
