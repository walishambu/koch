import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ManageUsersComponent } from './components/manage-users/manage-users.component';
import { HierarchyComponent } from './components/hierarchy/hierarchy.component';

const routes: Routes = [
  { path: 'users', component: ManageUsersComponent },
  { path: 'hierarchy', component: HierarchyComponent },
  { path: '', redirectTo: '/users', pathMatch: 'full' },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
