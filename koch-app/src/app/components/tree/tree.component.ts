import { FlatTreeControl } from '@angular/cdk/tree';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import {
  MatTreeFlatDataSource,
  MatTreeFlattener,
} from '@angular/material/tree';
import { SchoolNode } from 'src/app/shared/models/user-model';

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}

/**
 * @title Tree with flat nodes
 */
@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css'],
})
export class TreeComponent implements OnChanges, OnInit {
  @Input() submitData: SchoolNode[] = [];

  private _transformer = (node: SchoolNode, level: number) => {
    return {
      expandable: !!node.children && node.children.length > 0,
      name: node.name,
      level: level,
    };
  };

  treeControl = new FlatTreeControl<ExampleFlatNode>(
    (node) => node.level,
    (node) => node.expandable
  )
  
  

  treeFlattener = new MatTreeFlattener(
    this._transformer,
    (node) => node.level,
    (node) => node.expandable,
    (node) => node.children
  );

  dataSource = new MatTreeFlatDataSource(this.treeControl , this.treeFlattener);

  ngOnChanges(change: SimpleChanges) {
    this.dataSource.data = change.submitData.currentValue;
  }
  constructor() {}

  ngOnInit() {
     this.dataSource.data = [{
      name: 'School1',
      children: [
        {name: 'Class1'},
        {name: 'A'},
        {name: 'Male'},
        {name: 'Shambu'}
      ]
    }]
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
}
