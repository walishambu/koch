import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/shared';
import { CommonModalComponent } from 'src/app/shared/commons/common-modal/common-modal.component';
import { UserModel } from 'src/app/shared/models/user-model';
import { UserApiService } from 'src/app/shared/services/user.service';
import { UserFacade } from 'src/app/state/users-state/user.facade';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css'],
})
export class ManageUsersComponent implements OnInit {
  userList: UserModel[] = [];
  public displayedColumns = ['fullName', 'age', 'gender', 'mobile', 'actions'];
  public dataSource = new MatTableDataSource<UserModel>();
  unsubscribe$:Subject<void> =new Subject<void>()
  constructor(
    private userService: UserApiService,
    public dialog: MatDialog,
    private userFacade: UserFacade
  ) {}

  ngOnInit(): void {
    this.userFacade.getUsers();
    this.userFacade.userList$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
      if (user) {
        this.userList = user;
      }
    });
  }

  deleteUser(userId: number): void {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      maxWidth: '400px',
      data: {
        title: 'Delete User!',
        message: 'Are you sure you  want to delete ?',
      },
      disableClose:true
    });

    dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {
        this.userFacade.deleteUser(userId);
      }
    });
  }

  openDialog(actionType: string, userData?: UserModel) {
    if (actionType === 'Save') {
      const dialogRef = this.dialog.open(CommonModalComponent, {
        width: '340px',
        data: {
          isEdit: false,
          title: 'Create User',
          users: this.userList,
          selectedUser: {},
        },
        disableClose:true
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          let data = {
            fullName: result.fullName,
            gender: result.gender,
            mobile: result.mobile,
            email: '',
            age: result.age,
          };
          this.userFacade.saveUser(data);
        }
      });
    } else {
      const dialogRef = this.dialog.open(CommonModalComponent, {
        width: '340px',
        data: {
          isEdit: true,
          title: 'Edit User',
          users: this.userList,
          selectedUser: userData,
        },
        disableClose:true
      });
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          let data = {
            id: userData?.id,
            fullName: result.fullName,
            gender: result.gender,
            mobile: result.mobile,
            email: '',
            age: result.age,
          };
          this.userFacade.updateUser(data);
        }
      });
    }
  }

  ngOnDestroy():void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
