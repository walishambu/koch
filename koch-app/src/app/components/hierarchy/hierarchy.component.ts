import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SchoolNode } from 'src/app/shared/models/user-model';
import { UserFacade } from 'src/app/state/users-state/user.facade';
import { SCHOOLS, CLASSES, SECTIONS, GENDER } from './../../shared';

@Component({
  selector: 'app-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.css'],
})
export class HierarchyComponent implements OnInit {
  schools: string[] = SCHOOLS;
  classes: string[] = CLASSES;
  sections: string[] = SECTIONS;
  genders: string[] = GENDER;

  hierarchyForm: FormGroup;
  hieararchyData:SchoolNode[] = [];

  constructor(private fb: FormBuilder, private facadeService: UserFacade) {}

  ngOnInit(): void {
    this.hierarchyForm = this.fb.group({
      school: ['', Validators.compose([Validators.required])],
      sclass: ['', Validators.compose([Validators.required])],
      section: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      fullName: ['', Validators.compose([Validators.required])],
    });
    this.setInitValue();
  }

  setInitValue() {
    this.hierarchyForm.patchValue({
      school: 'School1',
      sclass: 'Class1',
      section: 'A',
      gender: 'Boys',
    });
  }

  onFormSubmit() {
    if (this.hierarchyForm.valid) {
      this.hieararchyData = this.hierarchyForm.value
      this.facadeService.saveHieararchy(this.hierarchyForm.value);
      const { value: { school, sclass, section, gender, fullName}} = this.hierarchyForm;
      this.hieararchyData = [
        {
          name: school,
          children:[
            {name: sclass},
            {name: section},
            {name: gender},
            {name: fullName}
          ]
        }
      ]
    } else {
      return;
    }
  }

  
}
