import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EffectsModule } from '@ngrx/effects';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTreeModule } from '@angular/material/tree';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ManageUsersComponent } from './components/manage-users/manage-users.component';
import { HierarchyComponent } from './components/hierarchy/hierarchy.component';
import { CommonModalComponent } from './shared/commons/common-modal/common-modal.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store, StoreModule } from '@ngrx/store';
import * as fromUsers from './state/users-state/user.reducer';
import { UsersEffects } from './state/users-state/user.effects';
import { UserFacade } from './state/users-state/user.facade';
import { UserApiService } from './shared';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { ConfirmDialogComponent } from './shared/commons/confirm-dialog/confirm-dialog.component';
import { TreeComponent } from './components/tree/tree.component';

@NgModule({
  declarations: [
    AppComponent,
    ManageUsersComponent,
    HierarchyComponent,
    CommonModalComponent,
    ConfirmDialogComponent,
    TreeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatDialogModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatOptionModule,
    MatToolbarModule,
    MatTableModule,
    MatTreeModule,
    
    StoreModule.forFeature('users', fromUsers.reducer),
    StoreModule.forRoot([]),
    EffectsModule.forFeature([UsersEffects]),
    EffectsModule.forRoot([]),
  ],
  providers: [UserApiService, Store, UserFacade],
  bootstrap: [AppComponent],
  exports: [MatTableModule],
})
export class AppModule {}
