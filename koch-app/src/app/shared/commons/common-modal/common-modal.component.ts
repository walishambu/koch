import { Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { userQuery } from 'src/app/state/users-state/user.selector';
import { UserModel, uniqueValidator, SEX } from '../..';

@Component({
  selector: 'app-common-modal',
  templateUrl: './common-modal.component.html',
  styleUrls: ['./common-modal.component.css'],
})
export class CommonModalComponent implements OnInit {
  userForm: FormGroup;
  genders: string[] = SEX;
  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<CommonModalComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      isEdit: boolean;
      title: string;
      users: UserModel[];
      selectedUser: UserModel;
    }
  ) {}
  ngOnInit(): void {
    if (this.data.isEdit) {
      this.userForm = this.fb.group({
        fullName: [
          this.data.selectedUser.fullName,
          Validators.compose([
            Validators.required,
            Validators.pattern('^[a-zA-Z ]+$'),
          ]),
        ],
        age: [
          this.data.selectedUser.age,
          Validators.compose([Validators.maxLength(3)]),
        ],
        gender: [this.data.selectedUser.gender],
        mobile: [
          this.data.selectedUser.mobile,
          Validators.compose([Validators.maxLength(10)]),
        ],
      });
    } else {
      this.userForm = this.fb.group({
        fullName: [
          '',
          Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z ]+$')]),
        ],
        age: ['', Validators.compose([Validators.maxLength(3)])],
        gender: [''],
        mobile: ['', Validators.compose([Validators.maxLength(10)])],
      });
    }
  }

  saveChanges() {
    this.dialogRef.close(this.userForm.value);
  }

  close() {
    this.dialogRef.close();
  }

  isSaveDisabled(): boolean {
    this.isUniqueData();
    const { invalid } = this.userForm;
    return invalid;
  }

  isUniqueData() {
    const {
      value: { fullName, mobile },
    } = this.userForm;
    let userList: UserModel[] = [];
    if (this.data.isEdit) {
      userList = this.data.users.filter((eachUser) => {
        return eachUser.id !== this.data.selectedUser.id;
      });
    } else {
      userList = this.data.users;
    }

    let isUniqueName = userList.some((eachUser: UserModel) => {
      const name = eachUser.fullName;
      return name.toLowerCase().trim() === fullName.toLowerCase().trim();
    });

    let isUniqueMobile = userList.some((eachUser: UserModel) => {
      const eachMob = eachUser.mobile;
      return eachMob === mobile;
    });

    if (isUniqueMobile) {
      this.userForm.controls['mobile'].setErrors({
        incorrect: true,
        message: 'This Mobile number is already exist in the system',
      });
    }

    if (isUniqueName) {
      this.userForm.controls['fullName'].setErrors({
        incorrect: true,
        message: 'This Name is already exist in the system',
      });
    }
  }
}
