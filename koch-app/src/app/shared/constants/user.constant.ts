export const SCHOOLS = ['School1', 'School2', 'School3', 'School4', 'School5'];

export const CLASSES = [
  'Class1',
  'Class2',
  'Class3',
  'Class4',
  'Class5',
  'Class6',
  'Class7',
  'Class8',
  'Class9',
  'Class10',
];

export const SECTIONS = ['A', 'B', 'C', 'D', 'E'];
export const GENDER = ['Boys', 'Girls'];
export const SEX = ['Male', 'Female'];
