import { AbstractControl, ValidatorFn } from '@angular/forms';
import { UserModel } from '..';

export function uniqueValidator(userList: UserModel[]) {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    const { value } = control;
      const isNameUnique = userList.some((user: UserModel) => {
        const fullName = user.fullName;
        if (fullName.trim() === value) {
          return true
        } else {
            return false
        }
      });
      if (!isNameUnique) {
        return { error: false };
      }
    return null;
  };
}
