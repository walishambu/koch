export interface UserModel {
  id?: number;
  fullName: string;
  gender: string;
  mobile: string;
  age: number;
}

export interface SchoolNode {
  name: string;
  children?: SchoolNode[];
}
