import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { UserModel } from './../models/user-model';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserApiService {
  private selectedUser = new Subject<any>();
  userSelected = this.selectedUser.asObservable();
  baseUrl: string = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getUsers(): Observable<any> {
    return this.http.get(`${this.baseUrl}/users`);
  }

  getUser(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/users` + id);
  }

  saveUser(user: UserModel): Observable<UserModel> {
    return this.http.post<UserModel>(`${this.baseUrl}/users`, user);
  }

  updateUser(user: UserModel): Observable<any> {
    return this.http.put<UserModel>(`${this.baseUrl}/users/${user.id}`, user);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete<UserModel>(`${this.baseUrl}/users/${id}`);
  }

  saveHierachy(data:object): Observable<any> {
    return this.http.post<object>(`${this.baseUrl}/hierarchy`, data);
  }
}
