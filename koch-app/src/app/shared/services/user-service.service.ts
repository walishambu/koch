import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { UserModel } from '../models/user-model';
import { catchError, retry, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserServiceService {
  baseUrl = 'http://localhost:3000';
  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get<UserModel[]>(`${this.baseUrl}/users`).pipe(
      catchError(this.handleError)
    );
  }

  public createUser(data: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'my-auth-token'
      })
    };
    return this.http.post<UserModel>(`${this.baseUrl}/users`, data, httpOptions)
    .pipe(
      catchError(this.handleError)
    );
  }

 

  updateUser(userId: number, data:UserModel) {
    return this.http.put<{}>(`${this.baseUrl}/users/${userId}`,  data ).pipe(
      catchError(this.handleError)
    );
  }

  deleteUser(userId: number) {
    return this.http.delete<{}>(`${this.baseUrl}/users/${userId}`).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 0) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, body was: `, error.error);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
  
}
  


