# KochApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.10.

## Development server
Run `npm install` post cloning the app

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `json-server --watch db.json` to start fake json server `http://localhost:3000/`. 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

